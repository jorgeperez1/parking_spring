/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.service;

import com.mycompany.easyparking.logica.Registros;
import java.util.List;

/**
 *
 * @author JorgeLeoPérez
 */
public interface RegistroService {
    
    public abstract List<Registros> listAllRegistros(); //Método para listar
    
    public abstract Registros addRegistro(Registros registro); //Método para añadir registros
    
    
    
}
