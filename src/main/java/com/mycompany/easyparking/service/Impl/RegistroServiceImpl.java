/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.service.Impl;

import com.mycompany.easyparking.logica.Registros;
import com.mycompany.easyparking.repository.RegistroRepository;
import com.mycompany.easyparking.service.RegistroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author JorgeLeoPérez
 */
@Service("registroservice")
public class RegistroServiceImpl implements RegistroService  {
    
    @Autowired //inyección de dependencias
    @Qualifier("registrorepository")
    private RegistroRepository usuarioRepository;

    @Override
    public List<Registros> listAllRegistros() {
        return usuarioRepository.findAll();
    }

    @Override
    public Registros addRegistro(Registros registro) {
        return usuarioRepository.save(registro);
    }
    
}
