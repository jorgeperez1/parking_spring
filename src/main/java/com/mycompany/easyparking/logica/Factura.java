/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.logica;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 *
 * @author JorgeLeoPérez
 */
public class Factura {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private int id;
    private String placa;
    private String fecha_ingreso;
    private String fecha_salida;
    private String hora_ingreso;
    private String hora_salida;
    private Double valor;
    private int tarifa;

    public Factura(int id, String placa, String fecha_ingreso, String fecha_salida, String hora_ingreso, String hora_salida, Double valor, int tarifa) {
        this.id = id;
        this.placa = placa;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_salida = fecha_salida;
        this.hora_ingreso = hora_ingreso;
        this.hora_salida = hora_salida;
        this.valor = valor;
        this.tarifa = tarifa;
    }

    Factura() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public String getHora_salida() {
        return hora_salida;
    }

    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    //Guardar
    public String guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        String sql = "INSERT INTO facturas(id, placa, fecha_ingreso, fecha_salida, hora_ingreso, hora_salida,valor,tarifa) VALUES(?,?,?,?,?,?,?,?,?)";
        System.out.println("Factura guardada correctamente");
        return (sql);
    }
    //Consultar

    public String consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT id,placa,fecha_ingreso,fecha_salida,hora_ingreso,hora_salida,valor,tarifa FROM facturas WHERE id =?";
        return sql;
    }

    //Actualizar
    public String actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        String sql = "UPDATE facturas SET placa = ? ,fecha_ingreso = ? , fecha_salida = ? , hora_ingreso = ?, hora_salida = ?, valor = ? WHERE id =?";
        System.out.println("Factura actualizada correctamente");
        return sql;

    }
    //borrar

    public String borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        String sql = "DELETE FROM facturas WHERE id = ? ";
        System.out.println("Factura borrada correctamente");
        return sql;

    }

    @Override
    public String toString() {
        return "Factura{" + "id=" + id + ", placa=" + placa + ", fecha_ingreso=" + fecha_ingreso + ", fecha_salida=" + fecha_salida + ", hora_ingreso=" + hora_ingreso + ", hora_salida=" + hora_salida + ", valor=" + valor + ", tarifa=" + tarifa + '}';
    }

}
