/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.*;
/**
 *
 * @author JorgeLeoPérez
 */
@Component("registro") // Declaración para que spring boot sepa a que modelo hacemos referencia con un Autowired.
@Entity
@Table(name="registros")
public class Registros {

    @Autowired // Permite la conexión del JdbcTemplate a la base de datos.
    transient JdbcTemplate jdbcTemplate; // Instancia de JdbcTemplate el cual es el equivalente al statement de la clase Conexion
    
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;
    
    @Column(name="placa")
    private String placa;
    
    @Column(name="fecha_ingreso")
    private String fecha_ingreso;
    
    @Column(name="hora_ingreso")
    private String hora_ingreso;
    
    @Column(name="id_plaza")
    private int id_plaza;
    
    @Column(name="tarifa")
    private int tarifa;
    
    @Column(name="estado")
    private int estado;

    public Registros(int id, String placa, String fecha_ingreso, String hora_ingreso, int id_plaza, int tarifa, int estado) {
        this.id = id;
        this.placa = placa;
        this.fecha_ingreso = fecha_ingreso;
        this.hora_ingreso = hora_ingreso;
        this.id_plaza = id_plaza;
        this.tarifa = tarifa;
        this.estado = estado;

    }

    Registros() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public int getPlaza() {
        return id_plaza;
    }

    public void setPlaza(int id_plaza) {
        this.id_plaza = id_plaza;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getId_plaza() {
        return id_plaza;
    }

    public void setId_plaza(int id_plaza) {
        this.id_plaza = id_plaza;
    }

    //Guardar
    public String guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        String sql = "INSERT INTO registros(id, placa, fecha_ingreso, hora_ingreso,id_plaza,tarifa,estado) VALUES(?,?,?,?,?,?,?);";
        return (sql);
    }
    //Consultar

    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT id,placa,fecha_ingreso,hora_ingreso,id_plaza,tarifa,estado FROM registros WHERE placa = ?";
        List<Registros> registros = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Registros(
                        rs.getInt("id"),
                        rs.getString("placa"),
                        rs.getString("fecha_ingreso"),
                        rs.getString("hora_ingreso"),
                        rs.getInt("id_plaza"),
                        rs.getInt("tarifa"),
                        rs.getInt("estado")
                ), new Object[]{this.getPlaca()}
        );
        if (registros != null && registros.size() > 0) {
            this.setId(registros.get(0).getId());
            this.setPlaca(registros.get(0).getPlaca());
            this.setFecha_ingreso(registros.get(0).getFecha_ingreso());
            this.setHora_ingreso(registros.get(0).getHora_ingreso());
            this.setId_plaza(registros.get(0).getId_plaza());
            this.setTarifa(registros.get(0).getTarifa());
            this.setEstado(registros.get(0).getEstado());

            return true;
        } else {
            return false;
        }

//        while (rs2.next()) // 
//        {
//            this.setId(rs2.getInt("id"));
//            this.setPlaca(rs2.getString("placa"));
//            this.setFecha_ingreso(rs2.getString("fecha_ingreso"));
//            this.setHora_ingreso(rs2.getString("hora_ingreso"));
//            System.out.println("id: " + rs2.getString(1) + " Placa : " + rs2.getString(2)+ " Fecha Ingreso : " + rs2.getString(3) + " Hora Ingreso : " + rs2.getString(4)); 
//
//        }
//
//        if (this.getId() != 0) {
//            return true;
//        } else {
//            return false;
//        }
    }
    //Actualizar

    public String actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        //String sql = "UPDATE registros SET fecha_ingreso = '" + getFecha_ingreso() + "', hora_ingreso = '" + getHora_ingreso() + "' plaza = " + getPlaza() + "' tarifa = " + getTarifa() + "' estado = " + getEstado() + " WHERE placa =" + getPlaca() + ";";
        //String sql = "UPDATE registros SET fecha_ingreso = '" + getFecha_ingreso() + "', hora_ingreso = '" + getHora_ingreso() + "' plaza = " + getPlaza() + "' tarifa = " + getTarifa() + "' estado = " + getEstado() + " WHERE placa =" + getPlaca() + ";";
        //String sql = "UPDATE registros SET placa = '" + getPlaca() + "', fecha_ingreso = '" + getFecha_ingreso() + "', hora_ingreso = '" + getHora_ingreso() + "', plaza = '" + getPlaza() + "', tarifa = '" + getTarifa() + "', estado = " + getEstado() + " WHERE placa =" + getPlaca() + ";";
        String sql2 = "UPDATE registros SET estado = ? WHERE placa ? ";

        System.out.println("Registro actualizado correctamente");
        return (sql2);

    }
    // Borrar

    public String borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        String sql = "DELETE FROM registros WHERE id =?";
        System.out.println("Registro borrado correctamente");
        return (sql);

    }

    @Override
    public String toString() {
        return "Registros{" + "id=" + id + ", placa=" + placa + ", fecha_ingreso=" + fecha_ingreso + ", hora_ingreso=" + hora_ingreso + ", plaza=" + id_plaza + ", tarifa=" + tarifa + ", estado=" + estado + '}';
    }

}
