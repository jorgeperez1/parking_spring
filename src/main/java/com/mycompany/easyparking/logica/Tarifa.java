///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.easyparking.logica;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import java.sql.SQLException;
//
///**
// *
// * @author JorgeLeoPérez
// */
//public class Tarifa {
//
//    private int id;
//    private String tipo;
//    private Double valor_fraccion;
//    private Double valor_hora;
//    private Double valor_dia;
//
//    public Tarifa(int id, String tipo, Double valor_fraccion, Double valor_hora, Double valor_dia) {
//        this.id = id;
//        this.tipo = tipo;
//        this.valor_fraccion = valor_fraccion;
//        this.valor_hora = valor_hora;
//        this.valor_dia = valor_dia;
//    }
//
//    Tarifa() {
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getTipo() {
//        return tipo;
//    }
//
//    public void setTipo(String tipo) {
//        this.tipo = tipo;
//    }
//
//    public Double getValor_fraccion() {
//        return valor_fraccion;
//    }
//
//    public void setValor_fraccion(Double valor_fraccion) {
//        this.valor_fraccion = valor_fraccion;
//    }
//
//    public Double getValor_hora() {
//        return valor_hora;
//    }
//
//    public void setValor_hora(Double valor_hora) {
//        this.valor_hora = valor_hora;
//    }
//
//    public Double getValor_dia() {
//        return valor_dia;
//    }
//
//    public void setValor_dia(Double valor_dia) {
//        this.valor_dia = valor_dia;
//    }
//
//    @Override
//    public String toString() {
//        return "Tarifa{" + "id=" + id + ", tipo=" + tipo + ", valor_fraccion=" + valor_fraccion + ", valor_hora=" + valor_hora + ", valor_dia=" + valor_dia + '}';
//    }
//
//    // Guardar
//    public String guardar() throws ClassNotFoundException, SQLException {
//        //CRUD - C
//        String sql = "INSERT INTO tarifas(id, tipo, valor_fraccion, valor_hora, valor_dia) VALUES(????? );";
//        System.out.println("Tarifa  añadida correctamente");
//        return(sql);
//    }
//
//
//     //Consultar
////    public boolean consultar() throws ClassNotFoundException, SQLException {
////        //CRUD -R 
////        String sql = "SELECT id,tipo,valor_fraccion,valor_hora,valor_dia FROM tarifas WHERE id = ? "; 
////        while(rs.next()) // 
////        {
////          this.setId(rs.getInt("id"));
////          this.setTipo(rs.getString("tipo"));
////          this.setValor_fraccion(rs.getDouble("valor_fraccion"));
////          this.setValor_hora(rs.getDouble("valor_hora"));
////          this.setValor_dia(rs.getDouble("valor_dia"));
////          
////            
////        }
////        
////        if (this.getId() != 0){
////            return true;
////        } else {
////            return false;
////        }
////
////
////    }
//
//    //Actualizar
//    public void actualizar() throws ClassNotFoundException, SQLException {
//        // CRUD -U
//        Conexion con = new Conexion();
//        String sql = "UPDATE tarifas SET tipo = '" + getTipo() + "',valor_fraccion = " + getValor_fraccion() + ", valor_hora = " + getValor_hora() + ", valor_dia = " + getValor_dia() + " WHERE id =" + getId() + ";";
//        ResultSet rs = Conexion.ejecutarConsulta(sql);
//        System.out.println("Tarifa actualizada correctamente");
//
//    }
//    // Borrar
//
//    public void borrar() throws SQLException, ClassNotFoundException {
//        //CRUD -D
//        Conexion con = new Conexion();
//        String sql = "DELETE FROM tarifas WHERE id =" + getId() + ";";
//        ResultSet rs = Conexion.ejecutarConsulta(sql);   
//        System.out.println("Tarifa borrada correctamente");     
//
//
//    }
//
//}
