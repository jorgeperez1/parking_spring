/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author JorgeLeoPérez
 */
@Component("Plaza") // en SpringBoot los modelos pasan a ser Componentes
public class Plaza {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private int id;
    private String estado;

    public Plaza(int id, String estado) {
        this.id = id;
        this.estado = estado;
    }

    Plaza() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    // Guardar
    public String guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        String sql = "INSERT INTO plazas (id, estado) VALUES(??)";
        System.out.println("Plaza añadida correctamente");
        return(sql);
    }
    //Consultar

    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT id,estado FROM plazas WHERE plazas.estado=1";
        //String sql = "SELECT estado FROM plazas WHERE id = '" + this.getId() + "'";
        
         List<Plaza> plazas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Plaza(
                        rs.getInt("id"),
                        rs.getString("estado")
                        
                ), new Object[]{this.getId()}
        );
        if (plazas != null &&  plazas.size() > 0 ) {
            this.setId(plazas.get(0).getId());
            this.setEstado(plazas.get(0).getEstado());

            
            return true;
        }
        else {
            return false;
        }
    }


//        while (rs.next()) // 
//        {
//            this.setId(rs.getInt("id"));
//            this.setEstado(rs.getString("estado"));
//            System.out.println("Plaza: " + rs.getString(1) + " Estado : " + rs.getString(2));            
//
//        }
//
//        if (this.getId() != 0) {
//            return true;
//        } else {
//            return false;
//        }

    
        //Consultar

//    public String consultar2() throws ClassNotFoundException, SQLException {
//        //CRUD -R 
//        String sql = "SELECT id,estado FROM plazas WHERE plazas.estado=1";
//        return (sql);
//        //String sql = "SELECT estado FROM plazas WHERE id = '" + this.getId() + "'";
//
//        while (rs.next()) // 
//        {
//            this.setId(rs.getInt("id"));
//            this.setEstado(rs.getString("estado"));
//            System.out.println("Plaza: " + rs.getString(1) + " Estado : " + rs.getString(2));            
//
//        }
//
//        if (this.getId() != 0) {
//            return true;
//        } else {
//            return false;
//        }
//
//    }

    //Actualizar
    public String actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U

        String sql = "UPDATE plazas SET estado = ? WHERE id = ? ";
        System.out.println("Plaza actualizada correctamente");
        return(sql);

    }

    //borrar
    public String borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        String sql = "DELETE FROM plazas WHERE id = ? ";
        System.out.println("Plaza borrada correctamente");
        return(sql);

    }

    @Override
    public String toString() {
        return "Plaza{" + "id=" + id + ", estado=" + estado + '}';
    }

}
