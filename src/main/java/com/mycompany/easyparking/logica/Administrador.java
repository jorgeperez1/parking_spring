/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 *
 * @author JorgeLeoPérezMayorga
 */
@Component("Administrador") // Declaración para que spring boot sepa a que modelo hacemos referencia con un Autowired.
public class Administrador {

    Administrador u;

    @Autowired // Permite la conexión del JdbcTemplate a la base de datos.
    transient JdbcTemplate jdbcTemplate; // Instancia de JdbcTemplate el cual es el equivalente al statement de la clase Conexion

    private int idadministradores;
    private String nombre_admin;

    public Administrador(int idadministradores, String nombre_admin) {
        this.idadministradores = idadministradores;
        this.nombre_admin = nombre_admin;
    }

    Administrador() {
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int getIdadministradores() {
        return idadministradores;
    }

    public void setIdadministradores(int idadministradores) {
        this.idadministradores = idadministradores;
    }

    public String getNombre_admin() {
        return nombre_admin;
    }

    public void setNombre_admin(String nombre_admin) {
        this.nombre_admin = nombre_admin;
    }

    //Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idadministradores,nombre_admin FROM administradores WHERE idadministradores = ?";
        List<Administrador> administradores = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Administrador(
                        rs.getInt("idadministradores"),
                        rs.getString("nombre_admin")
                ), new Object[]{this.getIdadministradores()}
        );
        if (administradores != null && administradores.size() > 0) {
            this.setIdadministradores(administradores.get(0).getIdadministradores());
            this.setNombre_admin(administradores.get(0).getNombre_admin());

            return true;
        } else {
            return false;
        }

//        while (rs.next()) // 
//        {
//            this.setId(rs.getInt("idadministradores"));
//            this.setNombre(rs.getString("nombre_admin"));
//
//        }
//
//        if (this.getNombre() != "" && this.getNombre() != null) {
//            return true;
//        } else {
//            return false;
//        }
    }    
    
     @Override
    public String toString() {
        return "Administrador{" + "jdbcTemplate=" + jdbcTemplate + ", idadministradores=" + idadministradores + ", nombre_admin=" + nombre_admin + '}';
    }

}
