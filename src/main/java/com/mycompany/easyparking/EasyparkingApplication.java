package com.mycompany.easyparking;

import com.google.gson.Gson;
import com.mycompany.easyparking.logica.Administrador;
import com.mycompany.easyparking.logica.Registros;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication //donde está ubicado el main
@RestController //indica que la clase será un api rest

public class EasyparkingApplication {

    @Autowired
    Registros u;
    //JdbcTemplate jdbcTemplate; //instancia equivalente al statement de la clase conexión
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.

    public static void main(String[] args) {

        SpringApplication.run(EasyparkingApplication.class, args);
    }

    @GetMapping("/registro")
    public String consultarRregistro(@RequestParam(value = "placa", defaultValue = "") String placa) throws ClassNotFoundException, SQLException {
        u.setPlaca(placa);//    
        if (u.consultar()) {
            String res = new Gson().toJson(u);
            u.setId(0);
            u.setPlaca("");
            u.setPlaza(0);
            u.setHora_ingreso("");
            u.setEstado(0);
            return res;
        } else {
            return new Gson().toJson(u);
        }
    }

    @GetMapping("/login") //vsta en la que nos ubicamos

    public String hola(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "99") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);

    }

}
