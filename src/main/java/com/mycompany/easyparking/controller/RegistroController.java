/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.controller;

import com.mycompany.easyparking.service.RegistroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author JorgeLeoPérez
 */
@Controller
@RequestMapping("/record")
public class RegistroController {

    @Autowired
    @Qualifier("registroservice")
    private RegistroService registroService;
    
    @GetMapping("/list")
    public ModelAndView listAllRegistros() {

        ModelAndView mav = new ModelAndView("list");
        mav.addObject("registros", registroService.listAllRegistros());
        return mav;
                
  
    }

}
