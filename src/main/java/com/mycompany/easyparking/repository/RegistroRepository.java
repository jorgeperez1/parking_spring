/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparking.repository;

import com.mycompany.easyparking.logica.Registros;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author JorgeLeoPérez
 */
@Repository("registrorepository")
public interface RegistroRepository extends JpaRepository<Registros,Serializable> {
    
}
